import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

let users: User[] = [
  { id: 1, name: 'Coffee', price: 56 },
  { id: 2, name: 'Milk', price: 30 },
  { id: 3, name: 'Cake', price: 60 },
];
let lastUserId = 4;
@Injectable()
export class UsersService {
  create(createUserDto: CreateUserDto) {
    console.log({ ...CreateUserDto });
    const newUser: User = {
      id: lastUserId++,
      ...createUserDto,
    };

    users.push(newUser);
    return newUser;
  }

  findAll() {
    return users;
  }

  findOne(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return users[index];
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    //console.log('user ' + JSON.stringify(users[index]));
    //console.log('update ' + JSON.stringify(updateUserDto));
    const updateUser: User = {
      ...users[index],
      ...updateUserDto,
    };
    users[index] = updateUser;
    return updateUser;
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteUser = users[index];
    users.splice(index, 1);
    return deleteUser;
  }
  reset() {
    users = [
      { id: 1, name: 'Coffee', price: 56 },
      { id: 2, name: 'Milk', price: 30 },
      { id: 3, name: 'Cake', price: 60 },
    ];
    lastUserId = 4;
    return 'RESET';
  }
}
