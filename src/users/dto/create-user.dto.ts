import { IsNotEmpty, MinLength } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  @MinLength(2)
  name: string;

  @IsNotEmpty()
  price: number;
}
